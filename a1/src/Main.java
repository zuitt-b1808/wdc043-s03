import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        String[] games = {"Mario Odyssey","Super Smash Bros. Ultimate","Luigi's Mansion 3","Pokemon Sword","Pokemon Shield"};
//        games.put("Mario Odyssey", 50);
//        games.put("Super Smarsh Bros. Ultimate", 20);
//        games.put("Luigi's Mansion 3", 15);
//        games.put("Pokemon Sword", 30);
//        games.put("Pokemon Shield", 100);
        for(String game: games){
            System.out.println(game);
        }

        HashMap<String,Integer>
                stocks = new HashMap<>();
        stocks.put(games[0],50);
        stocks.put(games[1],20);
        stocks.put(games[2],15);
        stocks.put(games[3],30);
        stocks.put(games[4],100);
        System.out.println(stocks);

        stocks.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<String>();

        stocks.forEach((key,value) -> {
            if (value <= 30)
                topGames.add(key);
            System.out.println(key + " has been added to top games list!");
        });
        System.out.println("Our shop's top games:");
        System.out.println(topGames);
        }
    }